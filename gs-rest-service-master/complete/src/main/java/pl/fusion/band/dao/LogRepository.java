package pl.fusion.band.dao;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.fusion.band.model.*;

public interface LogRepository extends JpaRepository<Log, Long> {
	Data findById(BigInteger id);
	void deleteById(String name);
	
}
