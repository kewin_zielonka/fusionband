package pl.fusion.band;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pl.fusion.band.dao.AlertRepository;
import pl.fusion.band.dao.DataRepository;
import pl.fusion.band.dao.LogRepository;
import pl.fusion.band.model.Alert;
import pl.fusion.band.model.Data;
import pl.fusion.band.model.Log;

@RestController
@RequestMapping("/rest")
public class ApiController {

	@Autowired
	private DataRepository dataRepository;
	
	@Autowired
	private LogRepository log;
	
	@Autowired
	private AlertRepository alertRepository;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/ping", method = RequestMethod.GET)
	public boolean ping() {
		logger.info("session sync...");
		log.save(new Log("ping",null));
		return true;
	}

	@RequestMapping(value = "/dataAll", method = RequestMethod.GET)
	public List<Data> dataList() {
		log.save(new Log("dataAll",null));
		return dataRepository.findAll();
	}
	
	@RequestMapping(value = "/log", method = RequestMethod.GET)
	public List<Log> log() {
		return log.findAll();
	}
	
	@RequestMapping(value = "/alert", method = RequestMethod.GET)
	public List<Alert> alert() {
		return alertRepository.findAll();
	}

	@RequestMapping(value = "/data", method = RequestMethod.GET)
	public Data data(@RequestParam(value = "id", defaultValue = "0") BigInteger id) {
		log.save(new Log("data",id.toString()));
		return dataRepository.findById(id);
	}
	
	@RequestMapping(value = "/alertPost", method = RequestMethod.POST)
	public boolean alertPost(@RequestBody String input) {
		logger.info(input);
		Alert dataIn = new Gson().fromJson(input, Alert.class);
		Alert alert = alertRepository.findById(dataIn.getId());
		alert = dataIn;
		alertRepository.save(alert);
		log.save(new Log("alertPost",alert.toString()));
		return true;
	}

	@RequestMapping(value = "/dataPost", method = RequestMethod.POST)
	public boolean dataPost(@RequestBody String input) {
		logger.info(input);
		Data dataIn = new Gson().fromJson(input, Data.class);

		if (dataIn.getPulse() != null) {
			String pulse = dataIn.getPulse().replace("[", "").replace("]", "");
			String[] array = pulse.split(",");
			dataIn.setPulse(array[1].trim());
		}

		if (dataIn.getBattery() != null) {
			String battery = dataIn.getBattery().replace("[", "").replace("]", "");
			String[] array = battery.split(",");
			dataIn.setBattery(array[1].trim());
		}
		
		if (dataIn.getStep() != null) {
			String steps = dataIn.getStep().replace("[", "").replace("]", "");
			String[] array = steps.split(",");
			dataIn.setStep(array[1].trim());
			dataIn.setDistance(array[5].trim());
			dataIn.setKcal(array[9].trim());
		}
		
		dataIn.setDate(new Timestamp(System.currentTimeMillis()));
		logger.info("save to DB: "+dataIn.toString());
		dataRepository.save(dataIn);
		log.save(new Log("dataPost",dataIn.toString()));
		return true;
	}

	@RequestMapping(value = "/clear", method = RequestMethod.GET)
	public boolean clear() {
		log.save(new Log("clear",null));
		dataRepository.deleteAll();
		return true;
	}

}
