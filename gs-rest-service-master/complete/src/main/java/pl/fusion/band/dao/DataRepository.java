package pl.fusion.band.dao;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.fusion.band.model.*;

public interface DataRepository extends JpaRepository<Data, Long> {
	Data findById(BigInteger id);
	void deleteById(String name);
	
}
