package pl.fusion.band;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import pl.fusion.band.dao.AlertRepository;
import pl.fusion.band.dao.DataRepository;
import pl.fusion.band.dao.LogRepository;
import pl.fusion.band.model.Alert;
import pl.fusion.band.model.Data;
import pl.fusion.band.model.Log;

@Component
public class ScheduledTasks {
	@Autowired
	private DataRepository dataRepository;
	@Autowired
	private AlertRepository alertRepository;
	@Autowired
	private LogRepository log;

	// 10 min
	 @Scheduled(fixedRate = 10000 * 60)
	 //	@Scheduled(fixedRate = 10000)
	public void alertMaker() {
		log.save(new Log("start task", ""));
		List<Data> dataList = dataRepository.findAll();
		int sum = 0;
		int index = 0;
		Timestamp dateOut = null;
		List<Timestamp> testList = new LinkedList<>();

		List<Alert> alertList = alertRepository.findAll();

		for (Data data : dataList) {

			if (!alertList.isEmpty()) {
				if (alertList.get(alertList.size() - 1).getData_date().compareTo(data.getDate()) < 0) {
					if (data.getPulse() != null)
						sum += new Integer(data.getPulse().trim());
					index++;
					dateOut = data.getDate();
					testList.add(data.getDate());
				}
			} else {
				if (data.getPulse() != null)
					sum += new Integer(data.getPulse().trim());
				index++;
				dateOut = data.getDate();
				testList.add(data.getDate());
			}
		}
		
		if (index > 0) {
			Integer avg = sum / index;
			String type = "Tetno ok";
			if (avg < 60)
				type = "Tetno za niskie";
			if (avg > 100)
				type = "Tetno za wysokie";
			Alert alert = new Alert(new Timestamp(System.currentTimeMillis()), dateOut, 0, avg.toString(), type);
			alertRepository.save(alert);
			log.save(new Log("alert save", alert.toString()));
		}
	}
}