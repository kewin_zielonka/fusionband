package pl.fusion.band.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "data")
@NamedQuery(name = "Data.findAll", query = "SELECT d FROM Data d")
public class Data implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private BigInteger id;

	private Timestamp date;
	private String pulse;
	private String step;
	private String battery;
	private String distance;
	private String kcal;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getKcal() {
		return kcal;
	}

	public void setKcal(String kcal) {
		this.kcal = kcal;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getPulse() {
		return pulse;
	}

	public void setPulse(String pulse) {
		this.pulse = pulse;
	}

	public String getStep() {
		return step;
	}

	public void setStep(String step) {
		this.step = step;
	}

	public String getBattery() {
		return battery;
	}

	public void setBattery(String battery) {
		this.battery = battery;
	}

	public Data(BigInteger id, Timestamp date, String pulse, String step, String battery) {
		super();
		this.id = id;
		this.date = date;
		this.pulse = pulse;
		this.step = step;
		this.battery = battery;
	}

	public Data() {
	}

	@Override
	public String toString() {
		return "Data [id=" + id + ", date=" + date + ", pulse=" + pulse + ", step=" + step + ", battery=" + battery
				+ ", distance=" + distance + ", kcal=" + kcal + "]";
	}

}
