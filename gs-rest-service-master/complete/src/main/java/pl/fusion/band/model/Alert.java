package pl.fusion.band.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "alert")
@NamedQuery(name = "Alert.findAll", query = "SELECT a FROM Alert a")
public class Alert implements Serializable {

	private static final long serialVersionUID = 1231L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private BigInteger id;
	private Timestamp date;
	private Timestamp data_date;
	private Integer status;
	private String value;
	private String type;

	public Alert() {
		
	}
	public Alert(Timestamp date, Timestamp data_date, Integer status, String value, String type) {
		super();
		this.date = date;
		this.data_date = data_date;
		this.status = status;
		this.value = value;
		this.type = type;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Timestamp getData_date() {
		return data_date;
	}

	public void setData_date(Timestamp data_date) {
		this.data_date = data_date;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Alert [id=" + id + ", date=" + date + ", value=" + value + ", type=" + type + "]";
	}

}
