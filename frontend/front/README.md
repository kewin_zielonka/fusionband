# Laboratory

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
To run Laboratory module - put in browser console: sessionStorage.setItem('pageType','laboratory')
To run Examination module - put in browser console: sessionStorage.setItem('pageType','examination')
Set id of entity: sessionStorage.setItem('entityId','107')


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Production Build
ng build --output-hashing none  -prod -aot -vc -cc -dop --buildOptimizer



