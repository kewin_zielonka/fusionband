
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { AppConfiguration } from "../configuration/app-configuration";
import { HttpClient } from "@angular/common/http";
import { Data } from '../model/data';
import { Alert } from '../model/alert';

@Injectable()
export class ApiService {
    public url = "";
    public delay: number = 3;
    public data: Array<Data> = [];
    public dataAlert: Array<Alert> = [];
    public dataReady: EventEmitter<any> = new EventEmitter();


    constructor(public configuration: AppConfiguration, protected httpClient: HttpClient) {
        this.url = configuration.getConfiguration('url');
        this.downloadData();
        this.downloadAlert();
    }

    public getData(): Observable<Data[]> {
        return this.httpClient.get<Data[]>(this.url + "/dataAll");
    }

    public getAlert(): Observable<Alert[]> {
        return this.httpClient.get<Alert[]>(this.url + "/alert");
    }

    public downloadData() {
        this.getData().subscribe(data => {

            this.data = data;

            this.data.forEach(el => {
                el.date = new Date(el.date);
            })

            this.dataReady.emit(null);
        },
            err => {
                console.log(err);
            });
    }

    public downloadAlert() {
        this.getAlert().subscribe(data => {
            this.dataAlert = data;
            this.dataAlert.forEach(el => {
                el.date = new Date(el.date);
                el.data_date = new Date(el.data_date);
            })
        },
            err => {
                console.log(err);
            });
    }

}