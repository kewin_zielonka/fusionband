import { RouterModule } from '@angular/router';
import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { TabStripModule } from '@progress/kendo-angular-layout';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { GridModule } from '@progress/kendo-angular-grid';
import { DialogModule } from '@progress/kendo-angular-dialog';

import { DatePickerModule } from '@progress/kendo-angular-dateinputs';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { MessageService } from "@progress/kendo-angular-l10n/dist/es/message.service";

import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { IntlModule } from '@progress/kendo-angular-intl';

import { registerLocaleData } from '@angular/common';
import localePl from '@angular/common/locales/pl';
import localePlExtra from '@angular/common/locales/extra/pl';

import { ApiService } from "../../services/api.service";

import { NgxEditorModule } from 'ngx-editor';

import { TestComponent } from "./test.component";
import { routes } from "../test-module/test.router";
import { ConfigComponent } from './config/config.component';
import { AlertsComponent } from './alerts/alerts.component';
import { ChartsComponent } from './charts/charts.component';
import { ChartsModule } from 'ng2-charts';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ButtonsModule,
    InputsModule,
    GridModule,
    DialogModule,
    TabStripModule,
    NgxEditorModule,
    DatePickerModule,
    DateInputsModule,
    IntlModule,
    LayoutModule,
    DropDownsModule,
    HttpModule,
    FormsModule,
    ChartsModule
  ],
  declarations: [
    TestComponent, ConfigComponent, AlertsComponent, ChartsComponent
  ], providers: [ApiService, DatePipe]
})
export class TestModule { }
