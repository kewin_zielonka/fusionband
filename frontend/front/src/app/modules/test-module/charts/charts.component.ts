import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss']
})
export class ChartsComponent implements OnInit {
  @ViewChild('chart') public _chart;
  constructor(public apiService: ApiService, private datePipe: DatePipe) {
    this.apiService.dataReady.subscribe(item => {
      // this.prepareData(false);
    });
  }

  ngOnInit() {
  }

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: any[] = [''];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public dateFrom: Date = new Date("2018-01-01");
  public dateTo: Date = new Date();
  public dateFormat: string = 'dd-MM HH:mm';

  public dateSetStatus:boolean = false;

  public barChartData: any[] = [
    { data: [0], label: 'Puls' }
  ];

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  public prepareData(withDate: boolean) {
    let y: Array<Date> = [];
    let x: Array<number> = [];
    this.apiService.data.forEach(el => {
      // let time = el.date.getDay() + " " + el.date.getMonth() + " " + el.date.getFullYear() + " " + el.date.getHours();
      if (withDate) {
        if (el.date >= this.dateFrom && el.date <= this.dateTo) {
          y.push(el.date);
          x.push(el.pulse);
        }

      }
      else {
        y.push(el.date);
        x.push(el.pulse);
      }
    });
    this.barChartLabels.splice(0, this.barChartLabels.length)
    y.forEach(el => {
      this.barChartLabels.push(this.datePipe.transform(el, this.dateFormat));
    })


    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = x;
    this.barChartData = clone;

    // this.barChartData = [
    //   { data: x, label: 'Puls' }
    // ];
    // this._chart.update()
  }


  dateSet() {
    setInterval(() => {
      this.dateSetStatus = true;
      this.apiService.downloadData();
      this.apiService.downloadAlert();
      this.dateTo = new Date();
      this.prepareData(true)
    }, this.apiService.delay * 1000);


  }


}
