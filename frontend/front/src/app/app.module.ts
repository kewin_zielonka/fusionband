import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_INITIALIZER } from '@angular/core';

import { MessageService } from "@progress/kendo-angular-l10n/dist/es/message.service"; 
import { TabStripModule } from '@progress/kendo-angular-layout';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { GridModule } from '@progress/kendo-angular-grid';
import { DialogModule } from '@progress/kendo-angular-dialog';


import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { appRouter } from "./app.router";
import { AppConfiguration } from './configuration/app-configuration';
import { TestModule } from "./modules/test-module/test.module";


@NgModule({
    declarations: [
        AppComponent    ],
    imports: [
        appRouter,
        HttpClientModule,
        BrowserModule,
        HttpModule,
        BrowserAnimationsModule,
        FormsModule
    ],    
    providers: [         AppConfiguration,
                HttpClientModule,
                { provide: APP_INITIALIZER, useFactory: (config: AppConfiguration) => () => config.load(), deps: [AppConfiguration], multi: true }],      
    bootstrap: [AppComponent]
})
export class AppModule { }