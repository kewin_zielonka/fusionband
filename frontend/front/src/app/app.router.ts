import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from "./app.component";

export const router: Routes = [
  { path: 'test', loadChildren: './modules/test-module/test.module#TestModule' }
];

export const appRouter: ModuleWithProviders = RouterModule.forRoot(router);