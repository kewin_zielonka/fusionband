import { Inject, Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class AppConfiguration {
    public configuration: Object = null;
    public environment: Object = null;

    constructor(public http: Http) { }

    public getConfiguration(key: string) {
        return this.configuration[key];
    }

    public getEnvironmentVariable(key: string) {
        return this.environment[key];
    }

    public async load() {
        this.configuration = await import('../../environments/configuration/config.production.json');
    }
}