import { NavigationCancel, NavigationError, NavigationEnd, Router, NavigationStart } from '@angular/router';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { AppConfiguration } from "./configuration/app-configuration";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    public pageType: string = '';
    public loading: boolean = false;
    public loadingTitle: string = '';

    constructor(public router: Router) {
        this.initializeRouterEventCallback();
        this.initializeLoadingTitle(this.getPageTypeFromSession());
        this.router.navigate([this.pageType], { skipLocationChange: true });
    }

    public initializeRouterEventCallback(): void {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                this.loading = true;
            } else if (event instanceof NavigationEnd ||
                event instanceof NavigationError ||
                event instanceof NavigationCancel) {
                this.loading = false;
            }
        });
    }

    public getPageTypeFromSession(): string {
        // sessionStorage.setItem('pageType', 'examination')
            this.pageType = "test";

        return this.pageType;
    }

    public initializeLoadingTitle(tabName: string): void {

        this.loadingTitle = "Ładowanie aplikacji...";


    }
}