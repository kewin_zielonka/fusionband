export class Data {
    public id: number;
    public date: Date;
    public pulse: number;
    public step: number;
    public battery: number;
    public distance: number;
    public kcal: number;
}
