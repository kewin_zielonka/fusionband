export class Alert {
    public id: number;
    public date: Date;
    public data_date: Date;
    public type: string;
    public value: any;
    public status: number;
}
