import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';

let applicationMode = import('./environments/configuration/environment.json').then(mode => {

  if(mode['environment'] == 'production')
    enableProdMode();

  platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
});

