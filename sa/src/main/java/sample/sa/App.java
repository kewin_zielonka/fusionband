package sample.sa;

import com.xuggle.mediatool.IMediaReader;
import com.xuggle.mediatool.MediaListenerAdapter;
import com.xuggle.mediatool.ToolFactory;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
    }
}

class Play extends MediaListenerAdapter
{
  /**
   * Takes a media container (file) as the first argument, opens it,
   * plays audio as quickly as it can, and opens up a Swing window and
   * displays video frames with <i>roughly</i> the right timing.
   *  
   * @param args Must contain one string which represents a filename
   */

  public static void play()
  {
    
    // create a new mr. decode an play audio and video
    IMediaReader reader = ToolFactory.makeReader("rtsp://184.72.239.149/vod/mp4:BigBuckBunny_175k.mov");
    reader.addListener(ToolFactory.makeViewer());
    while(reader.readPacket() == null)
      do {} while(false);
    
  }

}
